// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package launch

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"strings"
	"time"

	"cloud.google.com/go/storage"
	"google.golang.org/api/iterator"
)

func refreshCache(ctx context.Context,
	actionsRepoBucket *storage.BucketHandle,
	logOnlySeveritylevels string,
	environment string,
	microserviceName string) (actionsRepository *actionsRepo, err error) {
	var repo actionsRepo
	repo.TriggerActions = make(triggerActions)
	query := &storage.Query{Prefix: ""}
	it := actionsRepoBucket.Objects(ctx, query)
	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return &repo, fmt.Errorf("it.Next() %v", err)
		}
		if strings.Contains(attrs.Name, ".json") {
			reader, err := actionsRepoBucket.Object(attrs.Name).NewReader(ctx)
			if err != nil {
				return &repo, fmt.Errorf("actionsRepoBucket.Object(attrs.Name).NewReader(ctx) %v", err)
			}
			defer reader.Close()
			b, err := io.ReadAll(reader)
			if err != nil {
				return &repo, fmt.Errorf("io.ReadAll(reader) %v", err)
			}
			var tActions triggerActions
			err = json.Unmarshal(b, &tActions)
			if err != nil {
				return &repo, fmt.Errorf("json.Unmarshal %v", err)
			}
			for trigger, newActions := range tActions {
				actions := repo.TriggerActions[trigger]
				for _, action := range newActions {
					action := action
					actions = append(actions, action)
				}
				repo.TriggerActions[trigger] = actions
			}
		}
	}
	repo.Version = time.Now()
	return &repo, nil
}
