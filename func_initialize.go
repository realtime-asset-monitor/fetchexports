// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package launch

import (
	"context"
	"fmt"
	"log"
	"time"

	"cloud.google.com/go/profiler"
	"cloud.google.com/go/storage"
	cepubsub "github.com/cloudevents/sdk-go/protocol/pubsub/v2"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/google/uuid"
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

var global Global
var ctx = context.Background()

// initialize is called by init() and enable tests
func initialize() {
	start := time.Now()
	log.SetFlags(0)
	global.CommonEv.InitID = fmt.Sprintf("%v", uuid.New())
	var err error
	var serviceEnv ServiceEnv

	err = envconfig.Process(microserviceName, &serviceEnv)
	if err != nil {
		log.Println(glo.Entry{
			MicroserviceName: microserviceName,
			Severity:         "CRITICAL",
			Message:          "init_failed",
			Description:      fmt.Sprintf("envconfig.Process %s %v", microserviceName, err),
			InitID:           global.CommonEv.InitID,
		})
		log.Fatalf("INIT_FAILURE %v", err)
	}
	global.serviceEnv = &serviceEnv
	global.CommonEv.MicroserviceName = microserviceName
	global.CommonEv.Environment = global.serviceEnv.Environment
	global.CommonEv.LogOnlySeveritylevels = global.serviceEnv.LogOnlySeveritylevels
	var ev glo.EntryValues
	ev.CommonEntryValues = global.CommonEv
	glo.LogInitColdStart(ev)

	if global.serviceEnv.ActionsRepoBucketName == "" {
		global.serviceEnv.ActionsRepoBucketName = global.serviceEnv.ProjectID + "-actionsrepo"
	}

	var env Env
	err = envconfig.Process("", &env)
	if err != nil {
		glo.LogInitFatal(ev, "envconfig.Process no prefix", err)
	}
	global.env = &env

	if serviceEnv.StartProfiler {
		err := profiler.Start(profiler.Config{
			ProjectID:            serviceEnv.ProjectID,
			Service:              microserviceName,
			ServiceVersion:       env.KRevision,
			DebugLogging:         false,
			NoGoroutineProfiling: true,
			NoAllocProfiling:     true,
		})
		if err != nil {
			glo.LogInitFatal(ev, "failed to start the profiler", err)
		}
	}

	storageClient, err := storage.NewClient(context.Background())
	if err != nil {
		glo.LogInitFatal(ev, "storage.NewClient", err)
	}
	global.actionsRepoBucket = storageClient.Bucket(global.serviceEnv.ActionsRepoBucketName)

	global.actionsRepo, err = refreshCache(ctx, global.actionsRepoBucket,
		global.serviceEnv.LogOnlySeveritylevels, global.serviceEnv.Environment,
		microserviceName)
	if err != nil {
		glo.LogInitFatal(ev, "refreshCache", err)
		log.Fatalf("INIT_FAILURE %v", err)
	}

	actionTransport, err := cepubsub.New(ctx,
		cepubsub.WithProjectID(global.serviceEnv.ProjectID),
		cepubsub.WithTopicID(global.serviceEnv.ActionTopicID))
	if err != nil {
		glo.LogInitFatal(ev, "failed to create pubsub transport cepubsub.New", err)
	}

	global.actionClient, err = cloudevents.NewClient(actionTransport, cloudevents.WithTimeNow(), cloudevents.WithUUIDs())
	if err != nil {
		glo.LogInitFatal(ev, "cloudevents.NewClient", err)
	}

	glo.LogInitDone(ev, time.Since(start).Seconds())
}
