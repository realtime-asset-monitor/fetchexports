// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package launch

import (
	"bytes"
	"context"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"cloud.google.com/go/storage"
	"google.golang.org/api/iterator"
)

func TestIntegRefreshCache(t *testing.T) {
	var testCases = []struct {
		name                    string
		folderPath              string
		wantErrorMsgContains    string
		wantLogMsgContains      string
		wantTriggerCount        int
		wantTriggerActionCounts map[string]int
	}{
		{
			name:             "action01",
			folderPath:       "testdata/action01",
			wantTriggerCount: 3,
			wantTriggerActionCounts: map[string]int{
				"at_minute_0_past_every_3rd_hour":        2,
				"at_01am10_on_sunday":                    5,
				"at_00am00_on_day_of_month_1_in_january": 7,
			},
		},
		{
			name:             "action02",
			folderPath:       "testdata/action02",
			wantTriggerCount: 3,
			wantTriggerActionCounts: map[string]int{
				"at_minute_0_past_every_3rd_hour":        2,
				"at_01am10_on_sunday":                    5,
				"at_00am00_on_day_of_month_1_in_january": 7,
			},
		},
		{
			name:             "action03",
			folderPath:       "testdata/action03",
			wantTriggerCount: 3,
			wantTriggerActionCounts: map[string]int{
				"at_minute_0_past_every_3rd_hour":        4,
				"at_01am10_on_sunday":                    10,
				"at_00am00_on_day_of_month_1_in_january": 14,
			},
		},
		{
			name:                 "action04",
			folderPath:           "testdata/action04",
			wantErrorMsgContains: "json.Unmarshal invalid character",
		},
	}
	projectID := os.Getenv("LAUNCH_PROJECT_ID")
	if projectID == "" {
		t.Skip("Missing env var LAUNCH_PROJECT_ID")
	}
	ctx := context.Background()
	storageClient, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatalf("storage.NewClient %v", err)
	}
	actionsRepoBucketName := projectID + "-actionsrepo"
	actionsRepoBucket := storageClient.Bucket(actionsRepoBucketName)

	for _, tc := range testCases {
		tc := tc // https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(tc.name, func(t *testing.T) {
			query := &storage.Query{Prefix: ""}
			it := actionsRepoBucket.Objects(ctx, query)
			for {
				attrs, err := it.Next()
				if err == iterator.Done {
					break
				}
				if err != nil {
					log.Fatalf("it.Next() %s", err)
				}
				err = actionsRepoBucket.Object(attrs.Name).Delete(ctx)
				if err != nil {
					log.Fatalf("actionsRepoBucket.Object(attrs.Name).Delete(ctx) %s", err)
				}
				t.Logf("deleted test data %s %s", actionsRepoBucketName, attrs.Name)
			}
			err := filepath.Walk(tc.folderPath, func(path string, info os.FileInfo, err error) error {
				if filepath.Ext(path) == ".json" {
					p := filepath.Clean(path)
					if !strings.HasPrefix(p, "testdata/") {
						panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
					}
					b, err := os.ReadFile(p)
					if err != nil {
						log.Fatal(err)
					}
					storageObject := actionsRepoBucket.Object(filepath.Base(path))
					storageObjectWriter := storageObject.NewWriter(context.Background())
					_, err = fmt.Fprint(storageObjectWriter, string(b))
					if err != nil {
						return err
					}
					err = storageObjectWriter.Close()
					if err != nil {
						return err
					}
					t.Logf("find %s, added %s to %s", path, storageObject.ObjectName(), storageObject.BucketName())
				}
				return nil
			})
			if err != nil {
				log.Fatal(err)
			}

			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			rr, err := refreshCache(ctx,
				actionsRepoBucket,
				"INFO WARNING NOTICE CRITICAL", "dev", microserviceName)
			msgString := buffer.String()
			// t.Logf("%v", msgString)

			if tc.wantErrorMsgContains == "" {
				if err != nil {
					t.Errorf("want no errors and got %v", err)
				}
				if !strings.Contains(msgString, tc.wantLogMsgContains) {
					t.Errorf("want log msg contains %s and did not get it", tc.wantLogMsgContains)
				}
				if len(rr.TriggerActions) != tc.wantTriggerCount {
					t.Errorf("want %d triggers and got %d", tc.wantTriggerCount, len(rr.TriggerActions))
				}
				for triggerName, actionCount := range tc.wantTriggerActionCounts {
					if len(rr.TriggerActions[triggerName]) != actionCount {
						t.Errorf("trigger %s want %d actions and got %d", triggerName, actionCount, len(rr.TriggerActions[triggerName]))
					}
				}
			} else {
				if !strings.Contains(err.Error(), tc.wantErrorMsgContains) {
					t.Errorf("want error msg %s and got %s", tc.wantErrorMsgContains, err.Error())
				}
			}
			it = actionsRepoBucket.Objects(ctx, query)
			for {
				attrs, err := it.Next()
				if err == iterator.Done {
					break
				}
				if err != nil {
					log.Fatalf("it.Next() %s", err)
				}
				err = actionsRepoBucket.Object(attrs.Name).Delete(ctx)
				if err != nil {
					log.Fatalf("actionsRepoBucket.Object(attrs.Name).Delete(ctx) %s", err)
				}
				t.Logf("deleted test data %s %s", actionsRepoBucketName, attrs.Name)
			}

		})
	}
}
