# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#.

include:
  - project: "realtime-asset-monitor/_cicd"
    file: "/templates/variables.yml"

  - project: "realtime-asset-monitor/_cicd"
    file: "/templates/get_credentials.yml"

  - project: "realtime-asset-monitor/_cicd"
    file: "/templates/crun_deploy.yml"

  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml

variables:
  AUTHEN_MODE: --no-allow-unauthenticated
  CONCURRENCY: 80
  CPU: 1
  GOOGLE_FUNCTION_SIGNATURE_TYPE: cloudevent
  IMAGE_NAME: launch
  INGRESS: internal
  MAX_INSTANCES: 1000
  MEMORY: 128Mi
  SERVICE_NAME: launch
  TIMEOUT: --timeout=180s

stages:
  - test
  - build
  - deploy
  - hydrate

sast:
  stage: test

dependency_scanning:
  stage: test

go_test:
  stage: test
  image: google/cloud-sdk:slim
  id_tokens:
    GITLAB_OIDC_TOKEN:
      aud: https://gitlab.com
  before_script:
    - !reference [.get_credentials, before_script]
  script:
    - curl https://golang.org/dl/go${GO_VERSION}.linux-amd64.tar.gz -L --output go${GO_VERSION}.linux-amd64.tar.gz
    - tar -C /usr/local -xzf go${GO_VERSION}.linux-amd64.tar.gz
    - export PATH=$PATH:/usr/local/go/bin
    - go version
    - go test ./... -v -coverprofile=coverage.out
    - go tool cover -html=coverage.out -o coverage.html
    - go tool cover -func=coverage.out
  artifacts:
    paths:
      - coverage.html
    expire_in: 5 days
  coverage: "/\\(statements\\)\\s+\\d+.?\\d+%/"
  variables:
    LAUNCH_PROJECT_ID: ${DEV_PROJECT_ID}
    LAUNCH_LOG_ONLY_SEVERITY_LEVELS: ${LOG_ALL_SEVERITY_LEVELS}
    K_SERVICE: "tests"
    K_REVISION: tests-${CI_COMMIT_SHORT_SHA}
    PROJECT_ID: ${DEV_PROJECT_ID}
    PROJECT_NUMBER: ${DEV_PROJECT_NUMBER}
    SERVICE_ACCOUNT_EMAIL: ${DEV_RAM_BUILD_SA_EMAIL}
    WORKLOAD_IDENTITY_POOL_ID: ${DEV_WORKLOAD_IDENTITY_POOL_ID}
    WORKLOAD_IDENTITY_PROVIDER_ID: ${DEV_WORKLOAD_IDENTITY_PROVIDER_ID}

cbuild_pack:
  stage: build
  image: google/cloud-sdk:slim
  id_tokens:
    GITLAB_OIDC_TOKEN:
      aud: https://gitlab.com
  before_script:
    - !reference [.get_credentials, before_script]
  script:
    - gcloud builds submit
      --pack=^--^image=${ARTIFACT_REGISTRY}/${DEV_PROJECT_ID}/${REPOSITORY_NAME}/${IMAGE_NAME}:${VERSION}--builder=${CNB_BUILDER_IMAGE}--env=GOOGLE_FUNCTION_SIGNATURE_TYPE=${GOOGLE_FUNCTION_SIGNATURE_TYPE},GOOGLE_FUNCTION_TARGET=${GOOGLE_FUNCTION_TARGET}
      --project=${DEV_PROJECT_ID}
  variables:
    PROJECT_ID: ${DEV_PROJECT_ID}
    PROJECT_NUMBER: ${DEV_PROJECT_NUMBER}
    SERVICE_ACCOUNT_EMAIL: ${DEV_RAM_BUILD_SA_EMAIL}
    WORKLOAD_IDENTITY_POOL_ID: ${DEV_WORKLOAD_IDENTITY_POOL_ID}
    WORKLOAD_IDENTITY_PROVIDER_ID: ${DEV_WORKLOAD_IDENTITY_PROVIDER_ID}

notraffic_qa_deploy:
  stage: deploy
  image: google/cloud-sdk:slim
  id_tokens:
    GITLAB_OIDC_TOKEN:
      aud: https://gitlab.com
  before_script:
    - !reference [.get_credentials, before_script]
  script:
    - !reference [.crun_deploy, script]
  variables:
    PROJECT_ID: ${QA_PROJECT_ID}
    PROJECT_NUMBER: ${QA_PROJECT_NUMBER}
    SERVICE_ACCOUNT_EMAIL: ${QA_RAM_DEPLOY_SA_EMAIL}
    WORKLOAD_IDENTITY_POOL_ID: ${QA_WORKLOAD_IDENTITY_POOL_ID}
    WORKLOAD_IDENTITY_PROVIDER_ID: ${QA_WORKLOAD_IDENTITY_PROVIDER_ID}
    ENV_VARS: LAUNCH_ENVIRONMENT=qa,LAUNCH_PROJECT_ID=${QA_PROJECT_ID},MONITOR_START_PROFILER=true,LAUNCH_LOG_ONLY_SEVERITY_LEVELS=${LOG_ALL_SEVERITY_LEVELS}

100%_traffic:
  stage: hydrate
  image: google/cloud-sdk:slim
  id_tokens:
    GITLAB_OIDC_TOKEN:
      aud: https://gitlab.com
  before_script:
    - !reference [.get_credentials, before_script]
  script:
    - !reference [.crun_update_traffic, script]
  variables:
    PROJECT_ID: ${QA_PROJECT_ID}
    PROJECT_NUMBER: ${QA_PROJECT_NUMBER}
    SERVICE_ACCOUNT_EMAIL: ${QA_RAM_DEPLOY_SA_EMAIL}
    WORKLOAD_IDENTITY_POOL_ID: ${QA_WORKLOAD_IDENTITY_POOL_ID}
    WORKLOAD_IDENTITY_PROVIDER_ID: ${QA_WORKLOAD_IDENTITY_PROVIDER_ID}
    TRAFFIC_PERCENT: "100"

0%_traffic:
  stage: hydrate
  when: manual
  image: google/cloud-sdk:slim
  id_tokens:
    GITLAB_OIDC_TOKEN:
      aud: https://gitlab.com
  before_script:
    - !reference [.get_credentials, before_script]
  script:
    - !reference [.crun_update_traffic, script]
  variables:
    PROJECT_ID: ${QA_PROJECT_ID}
    PROJECT_NUMBER: ${QA_PROJECT_NUMBER}
    SERVICE_ACCOUNT_EMAIL: ${QA_RAM_DEPLOY_SA_EMAIL}
    WORKLOAD_IDENTITY_POOL_ID: ${QA_WORKLOAD_IDENTITY_POOL_ID}
    WORKLOAD_IDENTITY_PROVIDER_ID: ${QA_WORKLOAD_IDENTITY_PROVIDER_ID}
    TRAFFIC_PERCENT: "0"
