# launch

![pipeline status](https://gitlab.com/realtime-asset-monitor/launch/badges/main/pipeline.svg) ![coverage](https://gitlab.com/realtime-asset-monitor/launch/badges/main/coverage.svg?min_good=80)  
Fetch the compliance rules related to the asset type

Looks for the actions planned for a given Cloud Scheduler payload string received in the triggering request. For each action it publish a CloudEvent through a PubSub message so actions can be executed in parallel by the `execute` microservice. This architecture enable to mutualize the Cloud Scheduler meaning to reduce cost, and to scale action execution meaning improve capacity and efficiency.
